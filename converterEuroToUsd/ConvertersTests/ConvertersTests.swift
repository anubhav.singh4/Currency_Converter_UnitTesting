//
//  ConvertersTests.swift
//  ConvertersTests
//
//  Created by Anubhav-iOS on 11/11/22.
//

import XCTest

final class ConvertersTests: XCTestCase {

    // start function name with 'test' for test function
    func test_convert10_return1030(){
        let sut = Converters() // sut - system under testing
        
        let actual = sut.convertEuroToUSD(euro: "10")
        let expected = "$10.80"
        
        XCTAssertEqual(actual, expected)
    }
    
    func test_convertNegative10_returnsErrorMessage(){
        let sut = Converters() // sut - system under testing
        
        let actual = sut.convertEuroToUSD(euro: "-10")
        let expected = "Please enter positive value"
        
        XCTAssertEqual(actual, expected)
    }
    
    func test_convertBigValue_returnErrorMessage(){
        let sut = Converters()
        let actual = sut.convertEuroToUSD(euro: "10000")
        let expected = "Value is so big, please enter small value"
        
        XCTAssertEqual(actual, expected)
    }

}
