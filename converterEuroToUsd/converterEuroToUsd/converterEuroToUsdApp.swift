//
//  converterEuroToUsdApp.swift
//  converterEuroToUsd
//
//  Created by Anubhav-iOS on 11/11/22.
//

import SwiftUI

@main
struct converterEuroToUsdApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
