//
//  ViewModel.swift
//  converterEuroToUsd
//
//  Created by Anubhav-iOS on 11/11/22.
//

import Foundation
import UIKit

final class viewModel: ObservableObject {
    @Published var text = ""
    @Published var convertedText = "$0"
    
    private let converter = Converters()
    
    func convertMoney(){
        self.convertedText = converter.convertEuroToUSD(euro: text)
        self.hideKeyboard()
    }
    
    func hideKeyboard(){
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
