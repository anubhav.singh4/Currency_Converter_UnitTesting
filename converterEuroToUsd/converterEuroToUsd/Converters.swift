//
//  Converters.swift
//  converterEuroToUsd
//
//  Created by Anubhav-iOS on 11/11/22.
//

import Foundation

class Converters {
    
    func convertEuroToUSD(euro:String) -> String {
        let usdRate = 1.08
        let euroValue = Double(euro) ?? 0
        
        if euroValue <= 0 {
            return "Please enter positive value"
        }
        
        if euroValue > 1000 {
            return "Value is so big, please enter small value"
        }
        
        return "$\(String(format: "%.2f", euroValue * usdRate))"
    }
}
